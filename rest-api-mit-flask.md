# Eine eigene REST-API anbieten mit Hilfe des Python-Frameworks Flask

Als Beispiel soll eine API erstellt werden, die Daten von drei Sensoren zurückgibt. Wir starten mit einem einfachen Grundgerüst. Neben Python 3 benötigen wir das Framework Flask, das sich über den Paketmanager `pip` mit

```bash
$ pip install -U Flask
```
...installieren lässt

Das Ausgangsbeispiel gibt bei einem lesenden HTTP-Request (GET) ein "Hello World" zurück:
```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "HelloWorld"


if __name__ == '__main__':
    app.run()
```

Im Einzelnen:
- 	Das Framework Flask wird geladen mit  `from flask import Flask;` (Die anderen Imports werden erst später benötigt, wurde hier aber bereits gesetzt)

- Eine Instanz von Flask wird erstellt und zukünftig über die Referenz app angesprochen mit
`app = Flask(__name__)`

-	Im Folgenden wird eine Methode definiert, die auf HTTP-Requests reagieren soll. Dies wird über einen _Decorator_ implementiert (`@app.route('/')`). Dieser _Decorator_ ist eine Methode des Flask-Frameworks (`route()`), aus der heraus unsere Methode index() aufgerufen wird. Um die von uns implementierte Methode herum werden also die Operationen von route() dekoriert, die sich um die HTTP-Requests kümmern. Unsere Funktion muss also nur noch definieren, welche Antwort auf den Request gegeben wird, ohne sich um die HTTP-Details zu kümmern.

```python
@app.route('/')
def index():
    return "HelloWorld"
```

- Daraufhin wird der Flask-Server gestartet, wenn die Python-Datei ausgeführt wird:
```python
if __name__ == '__main__':
    app.run()
```

Von Haus aus startet Flask nur für `localhost` (IPv4: 127.0.0.1) und auf Port `5000`. Das kann aber individualisiert werden. Wenn der Aufruf im Browser über die lokale IP http://192.168.178.53:5000/ erfolgen soll müsste der Eintrag somit lauten:
`app.run(host='192.168.178.53', port=5001)`
(natürlich muss die jeweils dem Host vergebene IP eingesetzt werden!)

## Vorbereitung: Beispielressourcen festlegen

Unsere REST-API soll beispielhaft Sensoren und deren Werte verwalten. Die **Ressourcen** im Sinne von REST sind also Sensoren und deren Eigenschaften. Ressourcen bilden die Informationseinheiten einer REST-API. Jede Informationseinheit muss in einer REST-API über einen _Universal Resource Identifier_ (URI) ansprechbar sein (nicht wundern: im Deutschen schreibt sich "Ressource" mit _ss_, im englischen mit nur einem _s_).

Diese URI setzt sich zusammen aus Protokoll ("http://"), IP-Adresse oder Domainname ("127.0.0.1", "localhost"), Port (Flask-Standard: 5000) sowie dem individuellen Anteil jeder Ressource, in unserem Beispiel:

```python
ressource_uri="/api/sensors"
```

Als Ressourcen legen wir in einem Dictionary (der Key-Value-Sammlung in Python) für drei Sensoren Dummy-Werte an, die wir mit unserer REST-API verwalten wollen:

```python
sensors = {ressource_uri+"/1": {"id": 1,
                                "name": "Temperature",
                                "lastvalue": 35,
                                "values":ressource_uri+"/1"+"/values"},
           ressource_uri+"/2": {"id": 2,
                                "name": "Humidity",
                                "lastvalue": 75,
                                "values":ressource_uri+"/2"+"/values"},
           ressource_uri+"/3": {"id": 3,
                                "name": "CO2",
                                "lastvalue": 599,
                                "values":ressource_uri+"/3"+"/values"}}
```

## Liste aller Sensoren ausgeben

Diese Sensordaten wollen wir von unserer API ausgegeben bekommen, wenn wir die URI http://127.0.0.1:5000/api/sensors per GET-Request abfragen.

Die **Repräsentanz** dieser Ressourcen sollen im ersten Schritt JSON-formatierte Daten sein: wir wollen also einen JSON-String als Antwort erhalten, wenn wir die URI aufrufen. Damit wir nicht selbst die JSON-Formatierung übernehmen müssen, nutzen wir die Methode 'jsonify()' des Frameworks importieren - wir haben hier bereits alle Imports aufgeschrieben, die wir im weiteren Verlauf benötigen:

```python
from flask import Flask, jsonify, abort, request
from typing import Tuple
```

Die Methode hat den selben Aufbau wie das obige Beispiel, im _Decorator_ wird wieder die URI übergeben, für die diese Funktion zuständig ist - daneben wird noch das genutzte HTTP-Verb übergeben. Die Funktion gibt als Rückgabe den von `jsonify()` formatierten String zurück.

```python
@app.route(ressource_uri, methods=['GET'])
def get_sensors() -> str:
    """Read-Methode, gibt JSON aller Sensoren zurück"""
    return jsonify(sensors)
```

## GET-Requests an den Server senden:

GET-Requests lassen sich einfach per Browser absetzen, bei JSON erhält man meistens eine formatierte Ausgabe:

 ![Ansicht der Ressource http://127.0.0.1:5000/api/sensors im Browser](images/json-browser.png)

Wichtiger für das Testen der REST-Schnittstelle werden jedoch Konsolen Tools. In der Linux-Shell hilft uns das Tool curl:
```bash
>curl -i -X GET http://127.0.0.1:5000/api/sensors
```

Wir erhalten als Antwort sowohl den Header mit HTTP-Statuscode und weiteren Infos (Block oben) also auch den Content (JSON String unten)
```
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 270
Server: Werkzeug/1.0.1 Python/3.8.3
Date: Fri, 28 May 2021 05:43:31 GMT

{"/api/sensors/1":{"id":1,"lastvalue":35,"name":"Temp","values":"/api/sensors/1/values"},"/api/sensors/2":{"id":2,"lastvalue":75,"name":"Humidity","values":"/api/sensors/2/values"},"/api/sensors/3":{"id":3,"lastvalue":599,"name":"CO2","values":"/api/sensors/3/values"}}
```

In der PowerShell lautet das Cmdlet `Invoke-WebRequest`, mit dem wir die gleichen Informationen (etwas anders formatiert) erhalten:

```powershell
>Invoke-WebRequest http://localhost:5000/api/sensors -Method 'GET'
```
```
StatusCode        : 200
StatusDescription : OK
Content           : {"/api/sensors/1":{"id":1,"lastvalue":35,"name":"Temp","values":"/api/sensors/1/values"},"/api/sensors/2":{"id":2,"lastvalue"
                    :75,"name":"Humidity","values":"/api/sensors/2/values"},"/api/sensors/3":{"...
RawContent        : HTTP/1.0 200 OK
                    Content-Length: 270
                    Content-Type: application/json
                    Date: Fri, 28 May 2021 05:46:24 GMT
                    Server: Werkzeug/1.0.1 Python/3.8.3

                    {"/api/sensors/1":{"id":1,"lastvalue":35,"name":"Temp"...
Forms             : {}
Headers           : {[Content-Length, 270], [Content-Type, application/json], [Date, Fri, 28 May 2021 05:46:24 GMT], [Server, Werkzeug/1.0.1
                    Python/3.8.3]}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 270
```

## Eine einzelne Ressource ausgeben

Jede einzelne Ressource (hier: jeder Sensor) soll über einen eigenen URI verfügen, der per GET-Request Sensorinfos zurückgibt. Die jeweilige URI bildet sich dabei aus der Grund-URI der Sensoren (siehe oben) und der ID. Die Methode, die die Werte eines einzelnen Sensors übergibt, sieht beispielhaft so aus:

```python
@app.route(ressource_uri+'/<int:sensor_id>', methods=['GET'])
def get_sensor(sensor_id: int) -> str:
    """Read-Methode, gibt JSON eines Sensors zurück"""
    sensor_uri = ressource_uri+'/'+str(sensor_id)
    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")
    return jsonify(sensors[sensor_uri])
```

Wichtig bei REST-APIs ist, dass sich die Regeln, nach denen URIs gebildet werden, nicht erst aus Dokumentationen herausgelesen werden müssen. Die einzelnen Ressourcen sollen über Links miteinander verbunden sein. Diese Verknüpfung von Ressourcen wird in dem Schlagwort **Hypermedia** zusammengefasst. Dieses Grundprinzip von REST-APIs wird mit dem Akronym **HatEoAS** zusammengefasst: _Hypermedia as the Engine of Application State_. Wir haben bereits die Links auf die einzelnen Sensor-Ressourcen in der Liste oben zurückgegeben (z.B. `/api/sensors/1`). Um die ID des Sensors aus der aufgerufenen URI auszulesen, nutzen wir eine spezielle Notation des `route()`-_Decorators_:

```python
@app.route(ressource_uri+'/<int:sensor_id>', methods=['GET'])
```
Dieser erkennt das Muster und weist die Zahl (int) nach dem Querstrich der Variablen sensor_id zu, die wir als Argument unserer Funktion entgegennehmen (`def get_sensor(sensor_id: int) -> str:`) und so weiterverarbeiten können.

Am Ende geben wir den Eintrag unseres _Dictionaries_ `sensors` für den Schlüssel der jeweiligen URI JSON-formatiert zurück (`return jsonify(sensors[sensor_uri])`).

Bleibt nur noch die Frage, was getan werden soll, wenn die Ressource nicht existiert? Die Funktion prüft, ob ein Eintrag im Dictionary vorhanden ist und ruft andernfalls die Framework-Methode `abort()` auf, die den HTTP-Statuscode für nicht gefundene Ressourcen (404) sowie eine Fehlermeldung zurückgibt:


```python
    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")
```

Auch `abort` muss zunächst importiert werden, daher muss in der obersten Zeile wieder ergänzt werden (Falls noch nicht geschehen):

```python
from flask import Flask, jsonify, abort
```

Damit die Fehlermeldungen - analog zu den sonstigen Ressourcen, als JSON zurückgegeben werden, sollte noch eine weitere Methode ergänzt werden, die sich generell um alle Ressourcen kümmert, die nicht existieren. Es wird die Fehlermeldung und der passende HTTP-Statuscode (404) zurückgegeben.

```python
@app.errorhandler(404)
def resource_not_found(e) -> (str, int):
    return jsonify(error=str(e)), 404
```

## Eine neue Ressource erstellen

Die Funktion, um neue Ressourcen anzulegen, sieht im ganzen so aus:

```python
@app.route(ressource_uri+'/', methods=['POST'])
def create_sensor() -> Tuple[str, int]:
    """Create-Methode, erzeugt neuen Sensoreintrag"""
    if (not request.json) or ('name' not in request.json):
        abort(400, 'Es wurden keine interpretierbaren JSON-Ressourcen übergeben.')
    sensor_id = 0
    for uri,sensor in sensors.items():
        if int(sensor['id'])>sensor_id:
            sensor_id = sensor['id']
    sensor_id = sensor['id'] + 1
    sensor_uri = ressource_uri+'/'+str(sensor_id)
    sensors[sensor_uri] = {
        'id': sensor_id,
        'uri': sensor_uri,
        'name': request.json['name'],
        'lastvalue': request.json.get('lastvalue', ""),
        'values': sensor_uri+'/values'
    }
    return jsonify({'sensor': sensor_uri}), 201
```

Im Einzelnen besteht sie aus den folgenden Schritten:

* Damit die Type-Annotation `Tuple[str, int]` funktioniert muss diese oben importiert werden (`from typing import Tuple`)

* Damit der Request als Objekt vorliegt muss der Import ergänzt werden:  

```python
from flask import Flask, jsonify, abort, request
````

* Es muss geprüft werden, ob der übergebene JSON-Ausdruck existiert und alle eingabepflichtigen Informationen enthält
```python
if (not request.json) or ('name' not in request.json):
    abort(400, 'Es wurden keine interpretierbaren JSON-Ressourcen übergeben.')
```

* eine neue ID generiert weren (das Maximum der bestehenden IDs ermittelt werden)

```python
    sensor_id = 0
    for uri,sensor in sensors.items():
        if int(sensor['id'])>sensor_id:
            sensor_id = sensor['id']
    sensor_id = sensor['id'] + 1
```

* Der neue _Dictionary_-Eintrag wird zusammengestellt und gespeichert:

```python    
    sensor_uri = ressource_uri+'/'+str(sensor_id)
    sensors[sensor_uri] = {
        'id': sensor_id,
        'uri': sensor_uri,
        'name': request.json['name'],
        'lastvalue': request.json.get('lastvalue', ""),
        'values': sensor_uri+'/values'
    }
```

* ... und der neue URI wird zurückgegeben mit dem HTTP-Statuscode für _Resource created_ (201):

```python
    return jsonify({'sensor': sensor_uri}), 201
```

Ein POST-Aufruf ist über den Browser nicht mehr so einfach möglich, über die Linux-Shell werden die neuen Daten (als JSON) folgendermaßen übergeben:

```bash
$ curl -D - -X 'POST' -d '{"name": "NOx", "lastvalue": 35}' -H 'Content-Type: application/json' http://localhost:5000/api/sensors/
```

```
HTTP/1.0 201 CREATED
Content-Type: application/json
Content-Length: 28
Server: Werkzeug/1.0.1 Python/3.8.3
Date: Fri, 28 May 2021 07:38:39 GMT

{"sensor":"/api/sensors/4"}
```
Über die Powershell werden neue Ressourcen etwa so erzeugt:

```powershell
 Invoke-WebRequest http://localhost:5000/api/sensors/ -Method 'POST' -ContentType 'application/json; charset=utf-8'  -Body '{"name": "Pressure", "lastvalue": 997}'
```
```
StatusCode        : 201
StatusDescription : CREATED
Content           : {"sensor":"/api/sensors/6"}

RawContent        : HTTP/1.0 201 CREATED
                    Content-Length: 28
                    Content-Type: application/json
                    Date: Fri, 28 May 2021 07:52:35 GMT
                    Server: Werkzeug/1.0.1 Python/3.8.3

                    {"sensor":"/api/sensors/6"}

Forms             : {}
Headers           : {[Content-Length, 28], [Content-Type, application/json], [Date, Fri, 28 May 2021 07:52:35 GMT], [Server, Werkzeug/1.0.1
                    Python/3.8.3]}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 28
```

## Eine komplette Ressource updaten

In der Funktion zum Updaten einer bestehenden Ressource wiederholen sich die Codeblöcke aus den vorigen Funktionen:

```python
@app.route(ressource_uri+'/<int:sensor_id>', methods=['PUT'])
def update_sensor(sensor_id):
    """Update existing Sensor via PUT-Request"""

    sensor_uri = ressource_uri+'/'+str(sensor_id)

    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")

    if (not request.json):
        abort(400, 'Es wurden keine interpretierbaren JSON-Ressourcen übergeben.')

    sensors[sensor_uri]['name'] = request.json.get('name', sensors[sensor_uri]['name'])
    sensors[sensor_uri]['lastvalue'] = request.json.get('lastvalue', sensors[sensor_uri]['lastvalue'])
    return jsonify({'sensor': sensors[sensor_uri]})
```
Im Einzelnen:
* Der _Decorator_ wird so konfiguriert, dass er von einem PUT-Request getriggert wird:
```python
@app.route(ressource_uri+'/<int:sensor_id>', methods=['PUT'])
```

* Der URI wird zusammengestellt und geprüft, ob die Ressource überhaupt existiert:
```python
    sensor_uri = ressource_uri+'/'+str(sensor_id)

    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")
```

* wurde ein JSON-String im Body übergeben?

```python
    if (not request.json):
        abort(400, 'Es wurden keine interpretierbaren JSON-Ressourcen übergeben.')
```

* Falls bisher alles  geklappt hat, werden die übergebenen Werte aktualisiert (bzw. die bestehenden beibehalten)

```python     
    sensors[sensor_uri]['name'] = request.json.get('name', sensors[sensor_uri]['name'])
    sensors[sensor_uri]['lastvalue'] = request.json.get('lastvalue', sensors[sensor_uri]['lastvalue'])
```

* Es wird der aktualisierte JSON-String sowie der HTTP-Statuscode für eine erfolgreiche Operation (200) übergeben.

```python
    return jsonify({'sensor': sensors[sensor_uri]})
```

Um den Namen des zweiten Sensors anzupassen müsste in der Linux-Shell folgender Befehl abgesetzt werden:

```shell
$ curl -D - -d '{"name":"Raumtemperatur"}' -H 'Content-Type: application/json' -X PUT http://localhost:5000/api/sensors/2
```
```
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 92
Server: Werkzeug/1.0.1 Python/3.8.3
Date: Fri, 28 May 2021 08:01:10 GMT

{"sensor":{"id":2,"lastvalue":75,"name":"Raumtemperatur","values":"/api/sensors/2/values"}}
```
Mit der Powershell sieht das wie folgt aus:
```powershell
Invoke-RestMethod http://localhost:5000/api/sensors/2 -Method 'PUT' -ContentType 'application/json; charset=utf-8' -Body '{"name":"Ablufttemperatur"}'
```
```
sensor
------
@{id=2; lastvalue=75; name=Ablufttemperatur; values=/api/sensors/2/values}
```

## Löschen einer Ressource

Um Ressourcen löschen zu können wird die HTTP-Methode "DELETE" verwendet. Die entsprechende Funktion bindet diese wieder über den _Decorator_ ein. Die Überprüfung, ob die Ressource existiert erfolgt wie bereits oben beschrieben. Der eigentliche Löschvorgang wird durch die _Dictionary_-Methode `sensors.pop(sensor_uri)` ausgeführt, die die gelöschte Ressource als Rückgabewert weiterreicht.

```python
@app.route(ressource_uri+'/<int:sensor_id>', methods=['DELETE'])
def delete_sensor(sensor_id):
    sensor_uri = ressource_uri+'/'+str(sensor_id)
    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")
    return jsonify({'deleted_sensor': sensors.pop(sensor_uri)})
```

In der Shell lässt sich das folgendermaßen realisieren:

```bash
$ curl -D - -s -X DELETE http://localhost:5000/api/sensors/2
```

In unserer Implementierung wird das gelöschte Objekt zurückgegeben, denkbar wäre auch, lediglich den Statuscode "204" zurückzugeben (der besagt, das keine weiteren Daten angehängt sind, aber alles o.k. ist):

```
HTTP/1.0 200 OK
Content-Type: application/json
Content-Length: 94
Server: Werkzeug/1.0.1 Python/3.8.3
Date: Fri, 28 May 2021 08:20:44 GMT

{"deleted_sensor":{"id":2,"lastvalue":75,"name":"Humidity","values":"/api/sensors/2/values"}}
```

In der Powershell kann dies analog ausgeführt werden:

```PowerShell
Invoke-RestMethod http://localhost:5000/api/sensors/2 -Method 'DELETE'
```

```
deleted_sensor
--------------
@{id=1; lastvalue=35; name=Temp; values=/api/sensors/1/values}
```

Ob die Ressourcen wirklich gelöscht wurden kann mit einem anschließenden GET-Request geprüft werden.

### Fazit

Was noch fehlt ist die Authentifizierung an der Schnittstelle.


## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich in weiterbearbeitbarer Form im gitlab-Repository unter [https://gitlab.com/oer-informatik/python-basics/rest-api-with-flask]() und sind zur Nutzung als Open Education Resource (OER) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
