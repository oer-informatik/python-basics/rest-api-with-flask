from flask import Flask, jsonify, abort, request

ressource_uri = "/api/sensors"

sensors = {ressource_uri+"/1": {"id": 1,
                                "name": "Temp",
                                "lastvalue": 35,
                                "values": ressource_uri+"/1"+"/values"},
           ressource_uri+"/2": {"id": 2,
                                "name": "Humidity",
                                "lastvalue": 75,
                                "values": ressource_uri+"/2"+"/values"},
           ressource_uri+"/3": {"id": 3,
                                "name": "CO2",
                                "lastvalue": 599,
                                "values": ressource_uri+"/3"+"/values"}}

app = Flask(__name__)


@app.route('/')
def index():
    """Info, falls nur IP:Port eingegeben wird."""
    return "REST-API, die Sensordaten sammelt"


@app.route(ressource_uri, methods=['GET'])
def get_sensors() -> str:
    """Read-Methode, gibt JSON aller Sensoren zurück"""
    return jsonify(sensors)


@app.route(ressource_uri+'/<int:sensor_id>', methods=['GET'])
def get_sensor(sensor_id: int) -> str:
    """Read-Methode, gibt JSON eines Sensors zurück"""
    sensor_uri = ressource_uri+'/'+str(sensor_id)
    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")
    return jsonify(sensors[sensor_uri])


@app.route(ressource_uri+'/', methods=['POST'])
def create_sensor() -> (str, int):
    """Create-Methode, erzeugt neuen Sensoreintrag"""
    if (not request.json) or ('name' not in request.json):
        abort(400, 'keine interpretierbaren JSON-Ressourcen')
    sensor_id = 0
    for uri, sensor in sensors.items():
        if int(sensor['id']) > sensor_id:
            sensor_id = sensor['id']
    sensor_id = sensor['id'] + 1
    sensor_uri = ressource_uri+'/'+str(sensor_id)
    sensors[sensor_uri] = {
        'id': sensor_id,
        'uri': sensor_uri,
        'name': request.json['name'],
        'lastvalue': request.json.get('lastvalue', ""),
        'values': sensor_uri+'/values'
    }
    return jsonify({'sensor': sensor_uri}), 201


@app.route(ressource_uri+'/<int:sensor_id>', methods=['PUT'])
def update_sensor(sensor_id):
    """Update existing Sensor via PUT-Request"""

    sensor_uri = ressource_uri+'/'+str(sensor_id)

    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")

    if (not request.json):
        abort(400, 'keine interpretierbaren JSON-Ressourcen')

    sensors[sensor_uri]['name'] = request.json.get('name', sensors[sensor_uri]['name'])
    sensors[sensor_uri]['lastvalue'] = request.json.get('lastvalue',
                                                        sensors[sensor_uri]['lastvalue'])
    return jsonify({'sensor': sensors[sensor_uri]})


@app.route(ressource_uri+'/<int:sensor_id>', methods=['DELETE'])
def delete_sensor(sensor_id):
    sensor_uri = ressource_uri+'/'+str(sensor_id)
    if sensor_uri not in sensors:
        abort(404, "Sensor wurde nicht gefunden")
    return jsonify({'deleted_sensor': sensors.pop(sensor_uri)})


@app.errorhandler(404)
def resource_not_found(e) -> (str, int):
    return jsonify(error=str(e)), 404


if __name__ == '__main__':
    app.run()
